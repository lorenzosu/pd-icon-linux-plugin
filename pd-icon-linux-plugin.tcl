# META NAME pd window icons for linux
# META DESCRIPTION Provides icons for Pd windows and patches (canvas) in linux
# META AUTHOR <Lorenzo Sutton> lorenzofsutton@gmail.com
package require Tk
package require pdwindow 0.1
package require pdtk_canvas 0.1
package require pd_bindings

rename pdtk_canvas_new pdtk_canvas_new_old

# simple function to set an icon (Tk docs...) with file existance check
proc ::set_an_icon {the_win icon_full_path} {
	if [file exists $icon_full_path] {
		#wm iconbitmap $the_win @$icon_full_path
		set im [image create photo -file $icon_full_path]
		wm iconphoto $the_win -default $im
	} else {
		pdtk_post "pd icons linux plugin: Could not find icon file: $icon_full_path"
	}

}

proc ::set_window_icon {} {
	# The icon for the window must be in the same dir as the plugin.
	set window_icon_path [append "" $::current_plugin_loadpath "/pd_window.gif"]
    ::set_an_icon .pdwindow $window_icon_path	
}

# This modifies the invocation of a new canvas window to add the icon (scroll)
proc pdtk_canvas_new {mytoplevel width height geometry editable} {
    set l [pdtk_canvas_place_window $width $height $geometry]
    set width [lindex $l 0]
    set height [lindex $l 1]
    set geometry [lindex $l 2]

    # release the window grab here so that the new window will
    # properly get the Map and FocusIn events when its created
    ::pdwindow::busyrelease
    # set the loaded array for this new window so things can track state
    set ::loaded($mytoplevel) 0
    toplevel $mytoplevel -width $width -height $height -class PatchWindow
    wm group $mytoplevel .
    $mytoplevel configure -menu $::patch_menubar

    # we have to wait until $mytoplevel exists before we can generate
    # a <<Loading>> event for it, that's why this is here and not in the
    # started_loading_file proc.  Perhaps this doesn't make sense tho
    event generate $mytoplevel <<Loading>>

    wm geometry $mytoplevel $geometry
    wm minsize $mytoplevel $::canvas_minwidth $::canvas_minheight

    # -------------------- PLUGIN BEGIN -------------------------------
    # These lines are the only modification of the plugin to this function!
    if {$::windowingsystem eq "x11"} {
        set window_icon_path \
		[append "" $::current_plugin_loadpath "/pd_canvas.gif"]
	::set_an_icon $mytoplevel $window_icon_path
    } else {
	pdtk_post "WARNING: pd-icon-linux plugin is only for linux/X11 !!"
    }
    # ---------------------   END plugin modifications ----------------

    set tkcanvas [tkcanvas_name $mytoplevel]
    canvas $tkcanvas -width $width -height $height \
        -highlightthickness 0 -scrollregion [list 0 0 $width $height] \
        -xscrollcommand "$mytoplevel.xscroll set" \
        -yscrollcommand "$mytoplevel.yscroll set"
    scrollbar $mytoplevel.xscroll -orient horizontal -command "$tkcanvas xview"
    scrollbar $mytoplevel.yscroll -orient vertical -command "$tkcanvas yview"
    pack $tkcanvas -side left -expand 1 -fill both

    # for some crazy reason, win32 mousewheel scrolling is in units of
    # 120, and this forces Tk to interpret 120 to mean 1 scroll unit
    if {$::windowingsystem eq "win32"} {
        $tkcanvas configure -xscrollincrement 1 -yscrollincrement 1
    }

    ::pd_bindings::patch_bindings $mytoplevel

    # give focus to the canvas so it gets the events rather than the window 	 
    focus $tkcanvas

    # let the scrollbar logic determine if it should make things scrollable
    set ::xscrollable($tkcanvas) 0
    set ::yscrollable($tkcanvas) 0

    # init patch properties arrays
    set ::editingtext($mytoplevel) 0
    set ::childwindows($mytoplevel) {}

    # this should be at the end so that the window and canvas are all ready
    # before this variable changes.
    set ::editmode($mytoplevel) $editable
}

# This is for the Pd window and is done as soon as the plugin is loaded
if {$::windowingsystem eq "x11"} {
	::set_window_icon;
} else {
	pdtk_post "WARNING: pd-icon-linux plugin is only for linux/X11 !!"
}

pdtk_post "pd-icon-linux loaded. Welcome to the modern age :)\n\n"
